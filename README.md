# About "simple-ga-py"

This program is a simple, terminal-based, coloured explenation of genetic algorithms. 
The whole source is written in python and not longer than 400 lines and shall demonstrate, 
how genetic algorithms can be used, in order to find an optimal solution to an arbitrary problem,
where the whereabouts of the optimization are not known.

The program uses a genetic algorithm to manipulate simple binary strings (called "Vectors"). 
The binary strings encode exactly three bytes and can, subsequently, be interpreted as RGB-values.
One binary string is determined to be the "target", which the various vectors try to imitate. 
They do so, by utilizing crossover-actions, mutations and mixtures of two parent vectors. 
One can define in the CLI, which actions are allowed and for how many epochs the genetic algorithm is executed.

Many options are provided, as well as a fitness function, which can be easily understood by reading the simple 
sourcecode of thsi program.

As the program is determined to be a minimal example, it is written in only one single file, can be executed by 
Python v3.5+ and requires only a single dependency for colour in the terminal. Oh, and a terminal with
colour support, of course. :-)

## Download and setup

Please follow the following instructions to start simple-ga-py on your computer.
A virtual environment is created to manage the dependencies, so that your 
system-wide package index is not polluted.

```
# Clone this repository: 
$ git clone https://gitlab.com/jliebers/simple-ga-py.git

# Change directory into the folder
$ cd simple-ga-py

# Create a virtual environment
$ python3 -m venv .venv

# Activate the virtual environment
$ source .venv/bin/activate

# Install the python-dependencies.
$ pip install -r requirements.txt

# Start the program.
$ python3 ga.py
```

After the first installation, the software then can be started 
by only activating the virtual environment:

``` 
$ cd simple-ga-py
$ source .venv/bin/activate
$ python3 ga.py
```

## Example Usage

A simple video, provided as "ga.mp4" in this repository, demonstrates the usage. 
Here, the program is started and a random target vector is created, as well as 10 random vectors.
Within 15 cycles, only due to recombination of the 10 random vectors and enabled mutations 
at a mutation rate of 0.3, a perfect solution (i.e. an identical target vector) was found by 
executing the genetic algorithm.