from __future__ import print_function
from colors import color
import os
import random
import time
import sys


# Global variables which are set by the main menu and which are read by various functions.
CROSSOVER_SIZE = 4
CROSSOVER_INDEXES = []
MUTATION_CHANCE = 0
STEPS = 100
MATCH = 0
VARIANT = 3
ELITISTIC = 0
_id = 0
VECTORS = []
TARGET = None


class ColorVector:
    def __init__(self):
        """Constructor of a new ColorVector. The ColorVector is initialized with random RGB values."""
        r = random.randint(0, 255)
        g = random.randint(0, 255)
        b = random.randint(0, 255)
        self.set_color(r, g, b)
        self.num = get_id()

    def __repr__(self):
        return color('0x' + self.get_hex(), '', '#' + self.get_hex())

    def set_color(self, r, g, b):
        """Explicitly sets the RGB-values of the ColorVector."""
        r = int(r)
        g = int(g)
        b = int(b)
        self.hexcolor = '{:02x}{:02x}{:02x}'.format(r, g, b)       # hex representation e.g. '4c7da0'
        self.bincolor = "{0:024b}".format(int(self.hexcolor, 16))  # 3 bytes (r, g, b)

    def get_bin(self) -> str:
        """Returns a binary representation of the ColorVector."""
        return self.bincolor

    def set_bin(self, binstr: str):
        """SEts the binary representation of the ColorVector from a binary string."""
        r = int(binstr[:8], 2)
        g = int(binstr[8:16], 2)
        b = int(binstr[16:], 2)
        self.set_color(r, g, b)

    def get_hex(self) -> str:
        """Returns a hexadecimal representation of the ColorVector."""
        return self.hexcolor

    def get_num(self) -> str:
        return str(self.num)

    def get_fitness_value(self):
        """Returns the value of `f`, the fitness-function. Currently, the Hamming Distance is used."""
        return self.hamming_distance()

    def hamming_distance(self):
        """Implements the Hamming distance: https://en.wikipedia.org/wiki/Hamming_distance (see online)"""
        global TARGET

        s1 = TARGET.get_bin()
        s2 = self.get_bin()

        diff = 0
        for i in range(0, len(s1)):
            if s1[i] == s2[i]:
                diff += 1
            else:
                diff -= 1
        return diff


def get_id():
    """Returns a unique, incremented ID."""
    global _id
    ret_id = _id
    _id += 1
    return ret_id


def crossover(v1: ColorVector, v2: ColorVector, crossover_size: int):
    """Performs a Crossover between two ColorVectors v1 and v2. The crossover_size denotes the size of the
    genome, which is crossed over from v1 to v2."""
    global CROSSOVER_INDEXES #fixme

    c_crossoverindex = 0
    if len(CROSSOVER_INDEXES) == 0:
        crossover_index = random.randint(0, len(v1))
    else:
        crossover_index = CROSSOVER_INDEXES[c_crossoverindex]

    child = ''

    for i in range(0, len(v1)):
        if i < crossover_index:
            child += v1[i]
        elif crossover_index <= i < (crossover_index + crossover_size):
            child += v2[i % len(v1)]
        elif i >= crossover_index+crossover_size:
            if c_crossoverindex < len(CROSSOVER_INDEXES):
                crossover_index = CROSSOVER_INDEXES[c_crossoverindex]
                c_crossoverindex += 1
            child += v1[i % len(v1)]

    return child


def mutate(v1):
    """Performs a mutation of a ColorVector."""
    global MUTATION_CHANCE
    if random.random() <= MUTATION_CHANCE:
        print("Mutation.")
        mut_index = random.randint(0, len(v1))
        new = ''

        for i in range(0, len(v1)):
            if i < mut_index:
                new += v1[i]
            elif i == mut_index:
                if v1[i] == '0':
                    new += '1';
                else:
                    new += '0'
            elif i > mut_index:
                new += v1[i % len(v1)]

        return new
    else:
        return v1


# "neighbour"
def variant1():
    global VECTORS
    VECTORS = sorted(VECTORS, key=lambda vector: vector.get_fitness_value(), reverse=True)

    # create crossover:
    children = []

    # Save parents (elitism)
    for i in range(0, ELITISTIC):
        children.append(VECTORS[i])

    for i in range(0, len(VECTORS) - 1, 2):
        father = VECTORS[i]
        mother = VECTORS[i + 1]

        c1_genome = crossover(father.get_bin(), mother.get_bin(), CROSSOVER_SIZE)
        c2_genome = crossover(mother.get_bin(), father.get_bin(), CROSSOVER_SIZE)

        child1 = ColorVector()
        child1.set_bin(mutate(c1_genome))
        child2 = ColorVector()
        child2.set_bin(mutate(c2_genome))

        children.append(child1)
        children.append(child2)
    return children


# "master"
def variant2():
    global VECTORS
    VECTORS = sorted(VECTORS, key=lambda vector: vector.get_fitness_value(), reverse=True)

    # create crossover:
    children = []

    #elitism
    for i in range(0, ELITISTIC):
        children.append(VECTORS[i])

    j = 0
    while len(children) < len(VECTORS):
        if j == len(VECTORS)-1:
            j = 0

        father = VECTORS[0]
        j += 1
        mother = VECTORS[j]

        if mother.get_fitness_value() >= 0:
            child_genome = crossover(father.get_bin(), mother.get_bin(), CROSSOVER_SIZE)
            child = ColorVector()
            child.set_bin(mutate(child_genome))
            children.append(child)

    return children


# n/2 + random
def variant3():
    global VECTORS
    VECTORS = sorted(VECTORS, key=lambda vector: vector.get_fitness_value(), reverse=True)

    half = VECTORS[:int(len(VECTORS)/2)]
    children = []

    # elitism
    for i in range(0, ELITISTIC):
        children.append(VECTORS[i])

    while len(children) < len(VECTORS):
        rand_father = random.randint(0, len(half)-1)
        rand_mother = random.randint(0, len(half)-1)

        if rand_father == rand_mother: # same index must not be father and mother at the same time
            continue

        father = half[rand_father]
        mother = half[rand_mother]

        child_genome = crossover(father.get_bin(), mother.get_bin(), CROSSOVER_SIZE)
        c = ColorVector()
        c.set_bin(mutate(child_genome))
        children.append(c)

    return children


def experiment():
    global VECTORS, TARGET, ELITISTIC, CROSSOVER_SIZE, CROSSOVER_INDEXES, MUTATION_CHANCE, STEPS, MATCH, VARIANT

    print(" ++ Starting Experimentation Mode ++ ")
    if len(VECTORS) == 0 or len(VECTORS) % 2 == 1:
        print("The size of VECTORS is odd or zero. Aborting.")
        return

    user_elitisticinput = input("Q1/8: How many children can compete with their parents (elitistic)? "
                                "[Default: 0, provide int]  ")
    if user_elitisticinput == '':
        ELITISTIC = 0
    else:
        ELITISTIC = int(user_elitisticinput)

    user_crossoversize = input("Q2/8: What size should crossover have? [default: 4]  ")
    if user_crossoversize != '':
        CROSSOVER_SIZE = int(user_crossoversize)
        if CROSSOVER_SIZE < 0:
            print("Crossover size must be positive.")
            return

    user_crossindexes = input('Q3/8: To which parts should crossover apply? [default: <empty> = random, '
                              'provide csv-list of indexes, e.g. "0, 8"]  ')
    if user_crossindexes != '':
        user_crossindexes.replace(' ', '')  # remove whitespaces
        user_crossindexes = user_crossindexes.split(',')
        CROSSOVER_INDEXES.clear()
        if user_crossindexes[0] == "r":
            CROSSOVER_INDEXES = []
        else:
            for index in user_crossindexes:
                CROSSOVER_INDEXES.append(int(index))

    user_mutationchance = input("Q4/8: How rare are mutations? Enter float: [default: 0, 1 = every time]  ")
    if user_mutationchance != '':
        MUTATION_CHANCE = float(user_mutationchance)
        if MUTATION_CHANCE < 0 or MUTATION_CHANCE > 1:
            print("Mutation chance must be between zero and one.")
            return

    user_steps = input("Q5/8: For how many steps should the GA run? [default: 100, 0 = infinity]  ")
    if user_steps != '':
        STEPS = int(user_steps)
        if STEPS == 0:
            STEPS = sys.maxsize


    user_match = input("Q6/8: End the GA if a match of provided similarity is achieved? [default: 0, 0 = perfect match]  ")
    if user_match != '':
        MATCH = int(user_match)
        if MATCH < 0:
            print("Match-value must be zero or positive.")
            return

    user_stepbystep = input("Q7/8: Run the GA step by step? [default: Yes, No]  ")
    stepbystep = False
    if user_stepbystep == '' or user_stepbystep.lower() == 'Yes'.lower():
        stepbystep = True

    user_variant = input("Q8/8: Which variant? [default: 3; 1,2,3]  ")
    if user_variant != '':
        VARIANT = int(user_variant)


    print(" ++ STARTING EXPERIMENT ++ ")
    start_time = time.time()

    # -- START EXECUTION
    
    for step in range(0, STEPS):
        if (abs(TARGET.get_fitness_value() - VECTORS[0].get_fitness_value()) <= MATCH) or step > STEPS:
            return

        if VARIANT == 1:
            children = variant1()
        elif VARIANT == 2:
            children = variant2()
        elif VARIANT == 3:
            children = variant3()

        if(stepbystep):
            input("Press enter for next step ...")

        VECTORS = sorted(children, key=lambda vector: vector.get_fitness_value(), reverse=True)
        print("\nCycle "+str(step))
        print_details()

    print(" ++ Ending Experimentation Mode after " + str(step+1) + " generations (%s sec) ++ " % (time.time() - start_time))
    # -- END EXECUTION


def mainmenu():
    global VECTORS, TARGET

    while True:
        user_input = input("Enter command: ")
        inputs = user_input.split()

        if user_input == "":
            continue
        elif user_input.startswith("help") and len(user_input) < 6:
            print('List of available commands. Type "help <command>" for more information about a command.')
            print('\texp')
            print('\tprint <optional int>')
            print('\tlist')
            print('\tclear')
            print('\tprint <vectorNo, "target">')
            print('\tvec-add <int>')
            print('\tset-vec <int, "target"> <r> <g> <b>')
            print('\tvec-rm <int>')
            print('\texit')
            print('\thelp')
        elif user_input.startswith("exit"):
            exit(0)
        elif user_input.startswith("print"):
            if len(inputs) > 1: # print specific vector
                if inputs[1].lower() == "target":
                    print("Target vector: ", end='')
                    print(TARGET)
                elif inputs[1].isnumeric():
                    number = int(inputs[1])
                    print("Vector " + str(VECTORS[number].get_num()) + ": ", end='')
                    print(VECTORS[number], end='')
                    print(", bin: " + VECTORS[number].get_bin(), end='')
                    print(", f = " + str(VECTORS[number].get_fitness_value()))
            else:  # print all vectors
                print_details()
        elif user_input.startswith("list"):
            print("Target:  ", end='')
            print(TARGET)
            print("Vectors: ", end='')
            print(VECTORS)
        elif user_input.startswith("vec-add"):
            for i in range(int(inputs[1])):
                VECTORS.append(ColorVector())
        elif user_input.startswith("vec-set"):
            if inputs[1].lower() == "target":
                TARGET.set_color(inputs[2], inputs[3], inputs[4])
            else:
                VECTORS[int(inputs[1])].set_color(inputs[2], inputs[3], inputs[4])
        elif user_input.startswith("vec-rm"):
            VECTORS.remove(int(inputs[1]))
        elif user_input.startswith("clear"):
            VECTORS = []
        elif user_input.startswith("exp"):
            experiment()
        else:
            print("Command not recognized: " + user_input)


def print_details():
    print("Target (id=0): \t", end='')
    print(TARGET, end='')
    print(", bin: " + TARGET.get_bin(), end='')
    print(", f = " + str(TARGET.get_fitness_value()), end='')
    print("\t [unique id: 0]")
    for i in range(len(VECTORS)):
        print("Vector (id=" + str(i) + "):\t", end='')
        print(VECTORS[i], end='')
        print(", bin: " + VECTORS[i].get_bin(), end='')
        print(", f = " + str(VECTORS[i].get_fitness_value()), end='')
        print("\t [unique id: " + str(VECTORS[i].get_num()) + "]")


if __name__ == '__main__':
    """Main function. Prints a warning, if an environment variable indicates, that 24bit colour support is not 
    provided by the terminal."""
    TARGET = ColorVector()
    for i in range(10):
        VECTORS.append(ColorVector())

    print("Welcome to simple-ga-py 1.0!")
    print("Type \"help\" for help and information.")
    if not os.environ['COLORTERM'] == 'truecolor' or not os.environ['COLORTERM'] != '24bit':
        print("\nWarning: You need a compatible truecolour-terminal (24bit) to use this software.")
        print(
            "         You can find a list of compliant terminal emulators here: https://gist.github.com/XVilka/8346728")
        print("         Your terminal is probably not compliant. Proceed at your own risk.\n")

    mainmenu()
